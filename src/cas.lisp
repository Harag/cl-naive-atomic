(in-package :cl-naive-cas)

;;This code is taken from https://github.com/zerth/cl-cas-queue/blob/master/cas-queue.lisp

;;Copyright (c) 2014 Mike Watters under the MIT License.

;;Hacked slightly by Phil Marneweck in 2021 under the MIT License.


(defconstant +cas-sleep+ (/ 1 internal-time-units-per-second)
  "The initial duration in seconds for which a thread should sleep
while in a CAS retry loop.")

(defconstant +max-cas-sleep+ 0.1
  "The maximum duration in seconds for which a thread in a CAS retry
loop should sleep.")


#-(or ccl sbcl)
(defvar *global-cas-lock* (load-time-value (bt:make-lock "global cas lock")))

(defmacro compare-and-swap (&whole whole &environment env place old new)
  "Atomically attempt to set the new value of PLACE to be NEW if it
was EQ to OLD, returning non-nil if successful."
  (declare (ignorable whole env))
  ;; Note: ccl::conditional-store doesn't know how to use all the places.
  ;;       Only a few simple places are supported!
  #+ccl `(ccl::conditional-store ,place ,old ,new)
  #+sbcl (let ((ov (gensym "OLD")))
           `(let ((,ov ,old))
              (eq ,ov (sb-ext:compare-and-swap ,place ,ov ,new))))
  ;; #+lispworks `(sys:compare-and-swap ,place ,old ,new)
  #-(or ccl sbcl)
  (multiple-value-bind (vars vals store-vars writer-form reader-form) (get-setf-expansion place env)
    (warn "FIXME: please give a specific implementation of ~S on ~A; using bordeaux-thread locks"
          'compare-and-swap
          (lisp-implementation-type))
    (when (cdr store-vars)
      (warn "~S: Cannot compare-and-swap multiple-value place, only the first value is swapped"
            whole))
    (let ((vlock   (gensym "lock"))
          (vold    (gensym "old"))
          (vnew    (gensym "new"))
          (vresult (gensym "result")))
      `(bt:with-lock-held (*global-cas-lock*)
         ;; The lock must be around ,reader-form and ,writer-form
         ;; but also we want the evaluation of the place to be
         ;; atomic, so around ,vals.  And then, since old and new
         ;; must be evaluated, and could depend on the evaluation of
         ;; the place, we must also put them inside the lock.
         (let (,@(mapcar (function list) vars vals)
               (,(first store-vars) ,reader-form)
               (,vold ,old)
               (,vnew ,new)
               (,vresult nil))
           (when (eql ,(first store-vars) ,vold)
             (setf ,(first store-vars) ,vnew
                   ,vresult t)
             ,writer-form)
           ,vresult)))))

(defmacro atomic-incf (place &optional (increment 1))
  "Atomically increment the value of PLACE.
For CCL and SBCL it should be an accessor form for a struct slot holding an integer."
  #+ccl  `(ccl::atomic-incf-decf ,place ,increment)
  #+sbcl `(sb-ext:atomic-incf ,place ,increment)
  #+lispworks `(sys:atomic-incf ,place ,increment)
  #-(or ccl sbcl lispworks)
  (progn
    (warn "FIXME: please give a specific implementation of ~S on ~A; using bordeaux-thread locks"
          'atomic-incf
          (lisp-implementation-type))
    `(bt:with-lock-held (*global-cas-lock*)
       (incf ,place ,increment))))

(defmacro atomic-decf (place &optional (decrement 1))
  "Atomically decrement the value of PLACE.
For CCL and SBCL it should be an accessor form for a struct slot holding an integer."
  #+ccl  `(ccl::atomic-incf-decf ,place (- ,decrement))
  #+sbcl `(sb-ext:atomic-decf ,place ,decrement)
  #+lispworks `(sys:atomic-decf ,place ,decrement)
  #-(or ccl sbcl lispworks)
  (progn
    (warn "FIXME: please give a specific implementation of ~S on ~A; using bordeaux-thread locks"
          'atomic-decf
          (lisp-implementation-type))
    `(bt:with-lock-held (*global-cas-lock*)
         (decf ,place ,decrement))))

(defun retry-until (thunk timeout)
  "Calls THUNK repeatitively until it returns true, or timeout second
have elapsed, sleeping between each call a geometrically increasing
time interval from +CAS-SLEEP+ to +MAX-CAS-SLEEP+."
  (loop
    :with end  := (when timeout (+ timeout (get-universal-time)))
    :with wait := +cas-sleep+
    :if (funcall thunk)
      :do (return-from retry-until t)
    :until (and end (< (- end wait) (get-universal-time)))
    :do (sleep wait)
        (setf wait (min +max-cas-sleep+  (* 1.5 wait)))
    :finally (return nil)))

(defmacro cas-with-retry (place old new)
  "Does compare-and-swap until successful,
sleeping a geometrically increasing time (up to +MAX-CAS-SLEEP+) if not."
  `(retry-until (lambda () (compare-and-swap ,place ,old ,new)) nil))


(defstruct (cas-lock
            (:constructor make-cas-lock (&key &allow-other-keys)))
  (value nil))

(defstruct (cas-condition-variable
            (:constructor make-cas-condition-variable (&key &allow-other-keys)))
  (value nil))

(defparameter *default-lock-constructor*                'make-cas-lock)
(defparameter *default-condition-variable-constructor*  'make-cas-condition-variable)


(defmethod locking-thread ((lock cas-lock))
  (cas-lock-value lock))

(defmethod call-with-lock-held ((lock cas-lock) thunk)
  (cas-with-retry (cas-lock-value lock) nil (current-thread))
  (unwind-protect (funcall thunk)
    (compare-and-swap (cas-lock-value lock) (current-thread) nil)))


(defmethod condition-notify ((cv cas-condition-variable))
  (setf (cas-condition-variable-value cv) t))

(defmethod condition-wait ((lock cas-lock) (cv cas-condition-variable) &key timeout)
  (compare-and-swap (cas-lock-value lock) (current-thread) nil)
  (if timeout
      (let ((start (get-universal-time)))
        (and (retry-until
              (lambda ()
                (compare-and-swap (cas-condition-variable-value cv)   t   nil))
              timeout)
             (retry-until
              (lambda ()
                (compare-and-swap (cas-lock-value lock) nil (current-thread)))
              (- timeout (- (get-universal-time) start)))))
      (progn
        (cas-with-retry (cas-condition-variable-value cv)  t  nil)
        (cas-with-retry (cas-lock-value lock) nil (current-thread))
        t)))
