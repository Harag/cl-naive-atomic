(in-package :cl-naive-atomic.lock)

(defclass bt-lock ()
  ((lock   :initarg :lock
           :initform (bt:make-lock)
           :reader lock)
   (thread :initform nil
           :accessor locking-thread)))

(defun make-bt-lock (&key (name "Anonymous Lock") &allow-other-keys)
  (make-instance 'bt-lock :lock (bt:make-lock name)))

(defmethod call-with-lock-held ((lock bt-lock) thunk)
  (bt:with-lock-held ((lock lock))
    (setf (locking-thread lock) (bt:current-thread))
    (unwind-protect (funcall thunk)
      (setf (locking-thread lock) nil))))


(defclass bt-condition-variable ()
  ((condition-variable :initarg :condition-variable
                       :initform (bt:make-condition-variable)
                       :reader condition-variable)))

(defun make-bt-condition-variable (&key (name "Anonymous Condition Variable") &allow-other-keys)
  (make-instance 'bt-condition-variable
                 :condition-variable (bt:make-condition-variable :name name)))

(defmethod condition-notify ((cv bt-condition-variable))
  (bt:condition-notify (condition-variable cv)))

(defmethod condition-wait ((lock bt-lock) (cv bt-condition-variable) &key timeout)
  (bt:condition-wait (lock lock) (condition-variable cv) :timeout timeout))

