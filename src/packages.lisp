(in-package :common-lisp-user)

(defpackage :cl-naive-atomic.api
  (:use :cl)
  (:export
   :*default-mvar-constructor*  :make-mvar
   :+empty+
   :take
   :put
   :value
   :atomic-update)
  (:export
   :*default-lock-constructor*  :make-lock
   :locking-thread
   :call-with-lock-held
   :with-lock-held
   :*default-condition-variable-constructor* :make-condition-variable
   :condition-notify
   :condition-wait))

(defpackage :cl-naive-cas
  (:use :cl :cl-naive-atomic.api)
  (:import-from :bordeaux-threads
   :current-thread)
  (:export
   :+cas-sleep+
   :+max-cas-sleep+
   :compare-and-swap
   :cas-with-retry
   :atomic-incf
   :atomic-decf)
  (:export
   :cas-lock :make-cas-lock
   :cas-condition-variable :make-cas-condition-variable)
  (:export
   :*default-lock-constructor*  :make-lock
   :locking-thread
   :call-with-lock-held
   :with-lock-held
   :*default-condition-variable-constructor* :make-condition-variable
   :condition-notify
   :condition-wait))

(defpackage :cl-naive-atomic.lock
  (:use :cl :cl-naive-atomic.api)
  (:export
   :bt-lock :make-bt-lock
   :bt-condition-variable :make-bt-condition-variable)
  (:export
   :*default-lock-constructor*  :make-lock
   :locking-thread
   :call-with-lock-held
   :with-lock-held
   :*default-condition-variable-constructor* :make-condition-variable
   :condition-notify
   :condition-wait))

(defpackage :cl-naive-mvar
  (:use :cl :cl-naive-atomic.api :cl-naive-cas)
  (:export
   :mvar
   :mvar/cas   :make-mvar/cas  :mvar/cas-lock
   :mvar/lock  :make-mvar/lock)
  (:export
   :*default-mvar-constructor*  :make-mvar
   :+empty+
   :take
   :put
   :value
   :atomic-update))

(defpackage :cl-naive-atomic
  (:use :cl :cl-naive-atomic.api :cl-naive-mvar :cl-naive-cas)
  (:export
   :atomic)
  (:export
   :*default-mvar-constructor*  :make-mvar
   :+empty+
   :take
   :put
   :value
   :atomic-update))


(defpackage :cl-naive-channel
  (:use :cl :cl-naive-mvar
        :cl-naive-atomic.api)
  (:export
   :make-channel
   :make-unbounded-channel
   :read-from-channel
   :write-to-channel
   :take
   :put)
  (:export
   :make-consummer-channel
   :make-producer-channel
   :channel-pipe))
