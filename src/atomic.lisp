(in-package :cl-naive-atomic)

(defstruct (atomic
            (:include mvar/cas)))

(defmethod value ((mvar atomic))
  "Retrieve the value of the atomic container, without emptying it."
  (loop
     (let ((value +empty+))
       (cas-with-retry (mvar/cas-lock mvar) nil t)
       (rotatef value (slot-value mvar 'value))
       (compare-and-swap (mvar/cas-lock mvar) t nil)
       (if (eql value +empty+)
           (sleep 0)
           (return value))))

  (let ((value (cl-naive-mvar:take mvar)))
    (cl-naive-mvar:put mvar value)
    value))

(defmethod atomic-update ((atomic atomic) update-func)
  "Blocks any access to the atomic until the function passed to the it is done with its work. The update-func must take one argument which will be passed the current value of the atomic. The update-func must return a value to put back into the atomic."
  (let* ((value (cl-naive-mvar:take atomic))
	     (new-value (funcall update-func value)))
    (cl-naive-mvar:put atomic new-value)
    new-value))
