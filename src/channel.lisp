(in-package :cl-naive-channel)

;; https://www.oreilly.com/library/view/parallel-and-concurrent/9781449335939/ch07.html

;; (defun make-channel ())
;; (defun make-unbounded-channel ())

(defgeneric read-from-channel (channel))
(defgeneric write-to-channel (channel datum))

;;;
;;; 1-slot channel
;;;

(defstruct (channel
            (:constructor make-channel ()))
  (queue (make-mvar) :type mvar :read-only t))

(defmethod read-from-channel ((channel channel))
  (take (channel-queue channel)))

(defmethod write-to-channel ((channel channel) datum)
  (put (channel-queue channel) datum))

(defmethod take ((channel channel))
  (read-from-channel channel))

(defmethod put ((channel channel) datum)
  (write-to-channel channel datum))

;;;
;;; unbounded-slots channel
;;;

(defstruct (unbounded-channel
            (:constructor make-unbounded-channel (&aux
                                                    (hole (make-mvar))
                                                    (read-var  (make-mvar hole))
                                                    (write-var (make-mvar hole)))))
  read-var write-var)

(defmethod write-to-channel ((channel unbounded-channel) datum)
  (let ((new-hole (make-mvar))
        (old-hole (take (unbounded-channel-write-var channel))))
    (put old-hole (cons datum new-hole))
    (put (unbounded-channel-write-var channel) new-hole)
    datum))

(defmethod read-from-channel ((channel unbounded-channel))
  (let* ((stream (take (unbounded-channel-read-var channel)))
         (item   (take stream)))
    (put (unbounded-channel-read-var channel) (cdr item))
    (car item)))

(defmethod take ((channel unbounded-channel))
  (read-from-channel channel))

(defmethod put ((channel unbounded-channel) datum)
  (write-to-channel channel datum))


;; newChan :: IO (Chan a)
;; newChan = do
;;   hole  <- newEmptyMVar
;;   readVar  <- newMVar hole
;;   writeVar <- newMVar hole
;;   return (Chan readVar writeVar)
;;
;; writeChan :: Chan a -> a -> IO ()
;; writeChan (Chan _ writeVar) val = do
;;   newHole <- newEmptyMVar
;;   oldHole <- takeMVar writeVar
;;   putMVar oldHole (Item val newHole)
;;   putMVar writeVar newHole
;;
;; readChan :: Chan a -> IO a
;; readChan (Chan readVar _) = do
;;   stream <- takeMVar readVar
;;   Item val tail <- takeMVar stream
;;   putMVar readVar tail
;; return val


(defun make-consummer-channel (consummer &key (eof-value nil) (unbounded nil))
  (let ((channel (if unbounded
                     (make-unbounded-channel)
                     (make-channel))))
    (let ((thread (bt:make-thread (lambda ()
                                    (loop
                                      :for datum := (take channel)
                                      :until (eql datum eof-value)
                                      :do (funcall consummer datum))))))
      (values channel thread))))

(defun make-producer-channel (producer &key (eof-value nil) (unbounded nil))
  (let ((channel (if unbounded
                     (make-unbounded-channel)
                     (make-channel))))
    (let ((thread (bt:make-thread (lambda ()
                             (loop
                               :for datum := (funcall producer)
                               :do (put channel datum)
                               :until (eql datum eof-value))))))
      (values channel thread))))

(defun channel-pipe (producer consummer &key (eof-value nil) (unbounded nil) (wait nil))
  (let ((channel (if unbounded
                     (make-unbounded-channel)
                     (make-channel))))
    (let ((producer-thread
            (bt:make-thread (lambda ()
                              (loop
                                :for datum := (funcall producer)
                                :do (put channel datum)
                                :until (eql datum eof-value)))))
          (consummer-thread
            (bt:make-thread (lambda ()
                              (loop
                                :for datum := (take channel)
                                :until (eql datum eof-value)
                                :do (funcall consummer datum))))))
      (when wait
        (values (bt:join-thread producer-thread)
                (bt:join-thread consummer-thread))))))


;;;; THE END ;;;;
