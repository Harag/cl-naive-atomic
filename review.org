Code review
Pascal J. Bourguignon
Tue Jun 29 04:55:12 CEST 2021

* mvar
** defstruct mvar

We can use a BOA constructor:

#+BEGIN_SRC lisp
    (defstruct (mvar
                (:constructor make-mvar (&optional value)))
      "The struct is packed with a lock and two conditional variables which is read only and used by the system internally.

    The value slot is used to store data and starts out life with the constant +empty+."
      (mutex
       (bt:make-lock)
       :type bt:lock 
       :read-only t) 
      ;; signaled when reads are possible 
      (read-cvar
       (bt:make-condition-variable)
       ;; :type waitqueue 
       :read-only t) 
      ;; signaled when writes are possible 
      (write-cvar
       (bt:make-condition-variable)
       ;; :type waitqueue 
       :read-only t) 
      (value  +empty+))

  (make-mvar 42)
  --> #S(mvar :mutex #<recursive-lock "Anonymous lock" [ptr @ #x109900] #x3020028DB08D>
              :read-cvar #<ccl:semaphore #x3020028DB00D>
              :write-cvar #<ccl:semaphore #x3020028DAF8D>
              :value 42)
#+END_SRC

and avoid the %make-mvar / make-mvar dance.

** take

Why the timeout on condition-wait?
This will make TAKE return nil (which could be a value) after one second…

* cas
** compare-and-swap

Don't use EQ since that prevents storing numbers or characters!
The default comparison operator is EQL.

* naive-atomic
** value (setf value), atomic-update.

I fail to see how this is atomic.

Eg. with (value a) If another threads (put a v) between the (take a)
and the (put a), then the thread that called (value a) will be blocked
until some thread (take a).

If you want to prevent using take and put on atomic, then the
defstruct atomic should not include mvar (is-a) but have a mvar slot
(has-a).

Same for atomic-update.

* channels
** make-channel

The (let ((%channel channel)) …) dance is useless, the closure will
work as well with channel.

What is the STOP type?
Did you want to write (member stop) ?
Why not just (case value
               ((stop) …)
               (otherwise …)) ?

Oh, there's a STOP defstruct.  The STOP defstruct definition should be
put before make-channel.  But I don't see why the message (value)
should be an instance of STOP, since nothing in done in the channel
thread with it.


** stop function

The status is changed, but no message is sent to the channel. (thru the channel-queue).

** unbounded-channel

Initializing a slot of type mvar with nil is not wise… Either change
the type to (or null mvar), or initialize it with a mvar.
