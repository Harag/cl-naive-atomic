(defsystem "cl-naive-atomic.tests"
  :description "Tests for cl-naive-atomic"
  :version "2021.7.12.1" ; asd doesn't want leading 0s.
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ("cl-naive-atomic"
               "cl-naive-tests"
               "bordeaux-threads")
  :components (
	       (:file "tests/package")
               (:file "tests/test-util"    :depends-on ("tests/package"))
               (:file "tests/test-cas"     :depends-on ("tests/package"
                                                        "tests/test-util"))
               (:file "tests/test-mvar"    :depends-on ("tests/package"
                                                        "tests/test-util"))
               (:file "tests/test-lock"    :depends-on ("tests/package"
                                                        "tests/test-util"))
               (:file "tests/test-channel" :depends-on ("tests/package"
                                                        "tests/test-util"))))

