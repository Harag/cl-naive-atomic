(in-package :cl-naive-atomic.tests)


(testsuite cas-lock/with-lock-held
  (let ((lock (make-lock)))
    (testcase with-lock-held/result
              :expected 42
              :actual (with-lock-held (lock)
                        42))
    (testcase with-lock-held/return
              :expected 42
              :actual (block foo
                        (with-lock-held (lock)
                          (return-from foo 42))))))

(testsuite compare-and-swap
  #|TODO|# "test compare-and-swap basic feature"
  #|TODO|# "test compare-and-swap with multiple threads"
  )

(testsuite cas-with-retry
  #|TODO|# "test cas-with-retry with multiple-threads"
  )

(testsuite atomic-incf/decf
  #|TODO|# "test atomic-incf and atomic-decf with multiple-threads"
  )
