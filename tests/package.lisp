(in-package :common-lisp-user)

(defpackage :cl-naive-atomic.tests
  (:use
   :cl

   :cl-naive-atomic.api
   :cl-naive-cas
   :cl-naive-atomic.lock
   :cl-naive-mvar
   :cl-naive-atomic
   :cl-naive-channel

   :cl-naive-tests)

  (:export
   :run
   :report))
